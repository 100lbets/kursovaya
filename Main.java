import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import static javafx.scene.input.KeyCode.R;

public class Main {
    public static void main(String[] args) {
        File file = new File("Сужение.jpg"); //В кавычках вписывается путь к изображению знака, который распознается. Это должно быть одно из трех приложенных изображений
        File file1 = new File("Переезд.jpg");
        File file2 = new File("Переход.jpg");//эталонные изображения
        File file3 = new File("Сужение.jpg");
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(9000);//Создаем экземпляр класса ByteArrayOutputStream  для выходного потока для распознаваемого изображения
            BufferedImage img = ImageIO.read(file);
            ImageIO.write(img, "jpg", byteArrayOutputStream);
            byteArrayOutputStream.flush();
            byte[]array=byteArrayOutputStream.toByteArray(); //Создаем массив байт распознаваемого изображения на основе выходного потока
            
            ByteArrayOutputStream byteArrayOutputStream1 = new ByteArrayOutputStream(9000);
            BufferedImage img1 = ImageIO.read(file1);
            ImageIO.write(img1, "jpg", byteArrayOutputStream1);
            byteArrayOutputStream1.flush();
            byte[]array1=byteArrayOutputStream1.toByteArray();
            
            ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream(9000);
            BufferedImage img2 = ImageIO.read(file2);
            ImageIO.write(img2, "jpg", byteArrayOutputStream2); //Распознавание эталонных изображений
            byteArrayOutputStream2.flush();
            byte[]array2=byteArrayOutputStream2.toByteArray();
            
            ByteArrayOutputStream byteArrayOutputStream3 = new ByteArrayOutputStream(9000);
            BufferedImage img3 = ImageIO.read(file3);
            ImageIO.write(img3, "jpg", byteArrayOutputStream3);
            byteArrayOutputStream3.flush();
            byte[]array3=byteArrayOutputStream3.toByteArray();
            
            byte [][] bytes = new byte[3][array.length];//Для удобства обработки массивов байт в цикле объединяем массивы эталонных изображений в один двумерный массив байт
            bytes[0] = array1;
            bytes[1] = array2;
            bytes[2] = array3;
            double Rporog = 0.8;//коэффициент корреляции
            boolean flag = false;
            String[] sign = new String[3];
            sign[0] = "Железнодорожный переезд";
            sign[1] = "Пешеходный переход";//массив строк эталонных дорожных знаков
            sign[2] = "Сужение";
            //Рассчитываем коэффициент корреляции для массивов байт распознаваемого и i-того эталонного изображения
            for (int i = 0; i < 3; i++) {
                int Summ_x = 0;
                int Summ_y = 0;
                double Summ_xy = 0;
                double Summ_y_2 = 0;
                double Summ_x_2 = 0;
                double r = 0;

                for (int x = 0; x < array.length; x++){
                    Summ_x = Summ_x + array[x];
                }
                for (int y = 0; y < bytes[i].length; y++){
                    Summ_y = Summ_y + bytes[i][y];
                }
                double x_mean = Summ_x/array.length;
                double y_mean = Summ_y/bytes[i].length;
                for (int x = 0; x < bytes[2].length; x++){
                    Summ_xy = Summ_xy + (array[x] - x_mean)*(bytes[i][x] - y_mean);
                }
                for (int x = 0; x < bytes[2].length; x++){
                    Summ_x_2 = Summ_x_2 + (array[x] - x_mean)*(array[x] - x_mean);
                }
                for (int y = 0; y < bytes[2].length; y++){
                    Summ_y_2 = Summ_y_2 + (bytes[i][y] - y_mean)*(bytes[i][y] - y_mean);
                }
                r = (Summ_xy)/Math.sqrt(Summ_x_2*Summ_y_2);
                if (r >= Rporog){ //Проверяем коэффициент корреляции на превышение порогового значения
                    flag = true;
                    System.out.println(sign[i]);
                    break;
                }
            }
            if (flag == false) System.out.println("Данный знак отсутствует в базе данных");

            byteArrayOutputStream.close();
            byteArrayOutputStream1.close();
            byteArrayOutputStream2.close();
            byteArrayOutputStream3.close();
        }catch (IOException e){

        }

    }
}